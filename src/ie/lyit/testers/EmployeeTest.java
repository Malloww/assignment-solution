package ie.lyit.testers;


import ie.lyit.bank.Date;
import ie.lyit.bank.Employee;
import ie.lyit.bank.Name;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

/**
 * Class Junit to test Employee
 */
public class EmployeeTest
{

    //Create an employee
    Employee emp1  = new Employee(new Name("M","Luc","Damas"), new Date(02,06,2000), new Date(03,07,2010), 50000);

    /**
     * Test of the constructor
     */
    @Test
    public void Employee()
    {
        // Test of name
        assertEquals(emp1.getName().getTitle(), "M");
        assertEquals(emp1.getName().getFirstName(), "Luc");
        assertEquals(emp1.getName().getSurname(), "Damas");
        // Test date of birth
        assertEquals(emp1.getDateOfBirth().getDay(),02);
        assertEquals(emp1.getDateOfBirth().getMonth(),06);
        assertEquals(emp1.getDateOfBirth().getYear(), 2000);
        // Test start date
        assertEquals(emp1.getStartDate().getDay(),03);
        assertEquals(emp1.getStartDate().getMonth(),07);
        assertEquals(emp1.getStartDate().getYear(),2010);
        // Test salary
        assertEquals(emp1.getSalary(), 50000, 0.01);
    }

    /**
     * Test of the start date setter
     */
    @Test
    public void setStartDate()
    {
        Date newDate = new Date(06,02,2008);

        emp1.setStartDate(new Date(06,02,2008));
        assertEquals(emp1.getStartDate(),newDate);
    }

    /**
     * Test of the Salary setter
     */
    @Test
    public void setSalary()
    {
        double newSalary = 30000;

        emp1.setSalary(30000);
        assertEquals(emp1.getSalary(),newSalary,0.01);
    }

    /**
     * Test of toString
     */
    @Test
    public void testToString()
    {
        assertEquals(emp1.toString(), "M Luc Damas,2/6/2000Is the Employee n° 1 since 3/7/2010. His salary is 50000.0.");
    }


    /**
     * Test of the method who increment the salary
     */
    @Test
    public void incrementSalary()
    {
        double addSalary = 30000;
        emp1.incrementSalary(addSalary);
        assertEquals(emp1.getSalary(),80000.0,0.01);
    }

    /**
     * Test of the method who increment the salary when she return an exception
     */
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void incrementSalaryException()
    {
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("The salary can't be greater than €" + Employee.MAXIMUM_SALARY);
        double addSalary = 110000;
        emp1.incrementSalary(addSalary);
    }

    /**
     * Test of method who calculate the wage
     */
    @Test
    public void calculateWage()
    {
        double taxPercentage = 5;
        assertEquals(emp1.calculateWage(taxPercentage),3958.33,0.01);
    }
}