package ie.lyit.bank;

/**
 * Class of the object employee, an employee is a type of person
 */
public class Employee extends Person
{


    /*****Initisalisation******/

    public static final int MAXIMUM_SALARY = 150000;

    private Date startDate;
    private double salary;
    private int number;
    private static int nextNumber = 1;

    /*****Getter and Setter*****/

    public Date getStartDate()
    {
        return startDate;
    }

    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    public double getSalary()
    {
        return salary;
    }

    public void setSalary(double salary) throws IllegalArgumentException
    {
        if (salary > MAXIMUM_SALARY)
            throw new IllegalArgumentException("The salary can't be greater than €" + MAXIMUM_SALARY);
        this.salary = salary;
    }

    public int getNumber()
    {
        return number;
    }

    public static int getNextNumber()
    {
        return nextNumber;
    }

    /*****Constructor*****/

    /**
     * Default constructor
     */
    public Employee()
    {
        super(new Name(), new Date());
        this.startDate = new Date();
        this.salary = 0.00;
        this.number = nextNumber++;
    }

    /**
     * Constructor with all parameters
     * @param n The name of the Employee. Type Name
     * @param dob Date of birthday of the Employee. Type date
     * @param startDate Date when the Employee come in the company. Type date
     * @param salary The annual salary of the Employee. Type double
     */
    public Employee(Name n, Date dob, Date startDate, double salary) {
        super(n, dob);
        this.startDate = startDate;
        this.salary = salary;
        this.number = nextNumber++;
    }

    @Override
    public String toString() {
        return super.toString() + "Is the Employee n° " + number + " since " + startDate + ". His salary is " + salary + ".";
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        if (!super.equals(o))
            return false;

        Employee employee = (Employee) o;

        return Double.compare(employee.salary, salary) == 0 &&
                number == employee.number &&
                startDate.equals(employee.startDate);
    }

    /**
     * Methode who increment the salary depending of the amount given in parameter
     * @param incrementAmount The given amount
     * @throws IllegalArgumentException if the salary is > of the maximum parameter
     */
    public void incrementSalary(double incrementAmount) throws IllegalArgumentException
    {
        this.setSalary(this.getSalary()+incrementAmount);
    }

    /**
     * Method who calculate the mouth wage depending of a tax (in percentage) and the annual salary
     * @param taxPercentage The percentage of the tax
     * @return the wage calculated
     */
    public double calculateWage(double taxPercentage)
    {
        return (this.getSalary()/12)-((this.getSalary()/12)*(taxPercentage/100));
    }
}
